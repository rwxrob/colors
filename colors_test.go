package colors_test

import (
	"fmt"

	"gitlab.com/rwxrob/colors"
)

func ExampleHex2RGB() {
	rgb := colors.Hex2RGB("#2334ff")
	fmt.Println(rgb)
	rgb = colors.Hex2RGB("#23ff") // bad
	fmt.Println(rgb)
	// Output:
	// 35,52,255
}

func ExampleRGB2Hex() {
	rgb := colors.RGB2Hex("35,52,255")
	fmt.Println(rgb)
	rgb = colors.RGB2Hex("35, 52, 255")
	fmt.Println(rgb)
	rgb = colors.RGB2Hex("35 , 52, 255")
	fmt.Println(rgb)
	rgb = colors.RGB2Hex(" 35, 52, 255")
	fmt.Println(rgb)
	rgb = colors.RGB2Hex("35", "52", "255")
	fmt.Println(rgb)
	rgb = colors.RGB2Hex("35 ", " 52", " 255")
	fmt.Println(rgb)
	rgb = colors.RGB2Hex("35 ", " 52") // bad
	fmt.Println(rgb)
	// Output:
	// #2334FF
	// #2334FF
	// #2334FF
	// #2334FF
	// #2334FF
	// #2334FF
}
