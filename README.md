# Colors Utility Library and Command

[![GoDoc](https://godoc.org/gitlab.com/rwxrob/colors?status.svg)](https://godoc.org/gitlab.com/rwxrob/colors)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/rwxrob/colors)](https://goreportcard.com/report/gitlab.com/rwxrob/colors)
[![Coverage](https://gocover.io/_badge/gitlab.com/rwxrob/colors)](https://gocover.io/gitlab.com/rwxrob/colors)

Just a simple color conversion library and command.
