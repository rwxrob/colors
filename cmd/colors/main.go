package main

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/rwxrob/colors"
)

func main() {
	if len(os.Getenv("COMP_LINE")) > 0 {
		complete()
		return
	}
	switch {
	case len(os.Args) == 2:
		if os.Args[1] == "list" {
			list()
			return
		}
	case len(os.Args) > 2:
		switch os.Args[1] {
		case "hex2rgb":
			fmt.Println(colors.Hex2RGB(os.Args[2]))
		case "rgb2hex":
			fmt.Println(colors.RGB2Hex(os.Args[2:]...))
		default:
			usage()
			os.Exit(1)
		}
	default:
		usage()
		os.Exit(1)
	}
}

func complete() {
	if os.Args[2] == "" {
		list()
		return
	}
	commands := []string{"rgb2hex", "hex2rgb", "list"}
	for _, command := range commands {
		if strings.HasPrefix(command, os.Args[2]) {
			fmt.Println(command)
		}
	}
}

func list() {
	fmt.Print(`hex2rgb
rgb2hex
list
`)
}

func usage() {
	os.Stderr.WriteString(`usage:
  colors hex2rgb [#]RRGGBB
  colors rgb2hex (<red> <green> <blue> | <red>,<green>,<blue>)
  colors list
`)
}
