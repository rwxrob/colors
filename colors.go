/*
Package colors contains simple utilities functions for converting between string color values.
*/
package colors

import (
	"fmt"
	"strconv"
	"strings"
)

// Hex2RGB converts hex color string with optional initial octothorpe to
// RGB string separated by commas.
func Hex2RGB(hex string) string {
	if len(hex) < 6 {
		return ""
	}
	if len(hex) == 7 {
		hex = hex[1:]
	}

	// 0 means infer base from prefix
	r, _ := strconv.ParseInt("0x"+hex[0:2], 0, 32)
	g, _ := strconv.ParseInt("0x"+hex[2:4], 0, 32)
	b, _ := strconv.ParseInt("0x"+hex[4:6], 0, 32)

	return fmt.Sprintf("%v,%v,%v", r, g, b)
}

// RGB2Hex converts an RGB decimal value, either as a single string
// separated by commas, or as three separate strings, to a hex value
// prefixed with an octothorpe.
func RGB2Hex(args ...string) string {
	var r, g, b int
	switch len(args) {
	case 1:
		a := strings.Split(args[0], ",")
		r, _ = strconv.Atoi(strings.Trim(a[0], " "))
		g, _ = strconv.Atoi(strings.Trim(a[1], " "))
		b, _ = strconv.Atoi(strings.Trim(a[2], " "))
	case 3:
		r, _ = strconv.Atoi(strings.Trim(args[0], " "))
		g, _ = strconv.Atoi(strings.Trim(args[1], " "))
		b, _ = strconv.Atoi(strings.Trim(args[2], " "))
	default:
		return ""
	}
	return fmt.Sprintf("#%X%X%X", r, g, b)
}
